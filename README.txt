INTRODUCTION
------------
The image alt text module make the alt text or title text mandatory 
for upload images.


REQUIREMENTS
------------
This module requires the following modules:
 * imagefield (https://www.drupal.org/project/imagefield)
   check on the alt or title field in image field setting

INSTALLATION
------------
 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.
